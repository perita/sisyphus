import hxd.Rand;

using DungeonGenerator.RandExtender;
using Level.Tile;
using Party.Enemy;
using Level.Room;

class Sector {
	public final x:Int;
	public final y:Int;
	public var used:Bool;

	public function new(x:Int, y:Int) {
		this.x = x;
		this.y = y;
		used = false;
	}
}

class ForwardIntIterator {
	var min:Int;
	var max:Int;

	public inline function new(min:Int, max:Int) {
		this.min = min;
		this.max = max;
	}

	public inline function hasNext() {
		return min < max;
	}

	public inline function next() {
		return min++;
	}
}

class ReverseIntIterator {
	var min:Int;
	var max:Int;

	public inline function new(min:Int, max:Int) {
		this.min = min;
		this.max = max - 1;
	}

	public inline function hasNext() {
		return min >= max;
	}

	public inline function next() {
		return min--;
	}
}

class RandExtender {
	static public inline function range(r:Rand, min, max)
		return min + r.random(max - min + 1);

	static public inline function randBoolean(r:Rand, ?chance:Float = 0.5)
		return r.rand() <= chance;
}

class DungeonGenerator {
	public static function generateLevel(dungeon:Dungeon, width:Int, height:Int, floorNum:Int):Level {
		final sectorSize = 10;
		final halfSectorSize = Std.int(10 / 2);
		final sectors = [
			for (y in 0...Std.int(height / sectorSize)) [
				for (x in 0...Std.int(width / sectorSize))
					new Sector(x * sectorSize, y * sectorSize)
			]
		];
		final minRoom = 4;
		final maxRoom = 6;
		final rand = dungeon.rand;
		final numRooms = rand.range(5, 12);
		final level = new Level(dungeon, width, height);
		var prevRoom:Room = null;
		for (_ in 0...numRooms) {
			var sector:Sector;
			do {
				var row = sectors[rand.random(sectors.length)];
				sector = row[rand.random(row.length)];
			} while (sector.used);
			sector.used = true;
			final roomWidth = rand.range(minRoom, maxRoom);
			final roomHeight = rand.range(minRoom, maxRoom);
			final room = new Room(level, sector.x + halfSectorSize, sector.y + halfSectorSize, roomWidth, roomHeight);
			level.addRoom(room);
			connectRooms(level, room, prevRoom);
			prevRoom = room;
		}
		var roomNum = 1;
		var rooms = level.rooms;
		var numChests = rand.range(1, hxd.Math.iclamp(rooms.length - 4, 1, 3));
		for (_ in 0...numChests) {
			rooms[roomNum].content = Tile.ChestClosed(rand.range(2, 200));
			roomNum++;
		}
		for (room in level.rooms.slice(roomNum)) {
			final party = createEnemyParty(rand, floorNum);
			room.content = Enemy(party[0].enemy, party);
		}
		level.finish();
		return level;
	}

	static function createEnemyParty(rand:hxd.Rand, level:Int):Array<Party.Member> {
		final rn = rand.rand();
		final numMembers = if (rn < 0.75) {
			1;
		} else if (rn < 0.95) {
			2;
		} else {
			3;
		};
		final members = [];
		for (m in 0...numMembers) {
			final r = rand.rand();
			final member = if (level < 2) {
				new Party.Member(Rat, level);
			} else if (level < 5) {
				if (r < 0.4) {
					new Party.Member(Beetle, level);
				} else if (r < 0.6) {
					new Party.Member(Rat, level - 1);
				} else if (r < 0.8) {
					rand.randBoolean() ? new Party.Member(Rat, level) : new Party.Member(Beetle, level + 1);
				} else if (r < 0.92) {
					new Party.Member(Rat, level - 2);
				} else {
					new Party.Member(Spider, level + 2);
				}
			} else {
				if (r < 0.4) {
					new Party.Member(Bat, level);
				} else if (r < 0.6) {
					new Party.Member(Slime, level);
				} else if (r < 0.8) {
					new Party.Member(Snake, level);
				} else if (r < 0.92) {
					new Party.Member(Beetle, level);
				} else {
					new Party.Member(Jelly, level);
				}
			}
			members.push(member);
		}
		return members;
	}

	private static function connectRooms(level:Level, curRoom:Room, prevRoom:Null<Room>):Bool {
		if (prevRoom == null) {
			return false;
		}
		final curRoomX = curRoom.centerX;
		final curRoomY = curRoom.centerY;
		final prevRoomX = prevRoom.centerX;
		final prevRoomY = prevRoom.centerY;
		return addCorridor(curRoomX, prevRoomX, x -> level.cellAt(curRoomY, x))
			&& addCorridor(curRoomY, prevRoomY, y -> level.cellAt(y, prevRoomX));
	}

	private static function addCorridor(from:Int, to:Int, fetchCell:Int->Level.Cell):Bool {
		var outside = false;
		for (pos in corridor(from, to)) {
			final cell = fetchCell(pos);
			switch (cell.tile) {
				case Floor:
					if (outside)
						return false;
				case Empty:
					outside = true;
				default:
			}
			cell.tile = Floor;
		}
		return true;
	}

	private static inline function corridor(from:Int, to:Int):Iterator<Int> {
		return from < to ? new ForwardIntIterator(from, to + 1) : new ReverseIntIterator(from, to + 1);
	}
}

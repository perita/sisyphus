class Summary extends h2d.Object {
	public function new(roomsExplored:Int, totalRooms:Int, treasuresFound:Int, totalTreasures:Int, enemiesDefeated:Int, totalEnemies:Int, font:h2d.Font,
			?parent:h2d.Object) {
		super(parent);
		setPosition(10, 10);

		var backgroundScroll = hxd.Res.tiny_dungeon_interface.get("panel_scroll");
		var background = new h2d.ScaleGrid(backgroundScroll, 16, 16, 16, 16, this);
		background.width = Game.WorldWidth * Game.TileWidth - 20;
		background.height = Game.WorldHeight * Game.TileHeight - 48 - 20;

		var fontColor = 0xff333333;

		var title = new h2d.Text(font, this);
		title.text = "Level Cleared";
		title.textColor = fontColor;
		title.textAlign = Center;
		title.maxWidth = background.width;
		title.setPosition(0, 10);

		addSummaryLine(0, "Rooms Explored", roomsExplored, totalRooms, background.width, font, fontColor);
		addSummaryLine(1, "Enemies Defeated", enemiesDefeated, totalEnemies, background.width, font, fontColor);
		addSummaryLine(2, "Treasures Found", treasuresFound, totalTreasures, background.width, font, fontColor);

		final button = new Button("Descend", font, "scroll", this);
		button.setPosition(background.width / 2 - button.width / 2, background.height - button.height - 10);
		button.onClick = () -> onClose();
	}

	public dynamic function onClose() {}

	function addSummaryLine(row:Int, captionText:String, num:Int, denom:Int, maxWidth:Float, font:h2d.Font, fontColor:Int) {
		var y = row * 40 + 60;

		var caption = new h2d.Text(font, this);
		caption.text = captionText;
		caption.textColor = fontColor;
		caption.setPosition(10, y);

		var summary = new h2d.Text(font, this);
		summary.text = '${num} / ${denom}';
		summary.textColor = fontColor;
		summary.textAlign = Right;
		summary.maxWidth = maxWidth - 20;
		summary.setPosition(10, y + 10);
	}
}

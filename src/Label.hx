import h2d.Text.Align;

class Label extends h2d.Text {

	override function sync(ctx:h2d.RenderContext) {
		super.sync(ctx);
		alpha -= 0.03;
		setPosition(x, y - 0.1);
		if (alpha <= 0) {
			remove();
		}
	}
}
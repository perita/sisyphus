class Portrait extends h2d.Object {
	final background:h2d.Tile;
	final portrait:h2d.Tile;
	final xpBar:h2d.Tile;
	final group:h2d.TileGroup;

	public function new(?parent:h2d.Object) {
		super(parent);
		background = hxd.Res.tiny_dungeon_interface.get("panel_window");
		xpBar = hxd.Res.tiny_dungeon_interface.get("xp_full");
		portrait = hxd.Res.tiny_dungeon_portraits.get("berserker_3m");
		group = new h2d.TileGroup(background, this);
		experience = 0;
		rebuild();
	}

	public var experience(default, set): Int;

	public var height(get, null):Float;
	public var width(get, null):Float;

	function get_height():Float
		return background.height;

	function get_width():Float
		return background.width;

	function set_experience(xp:Int):Int {
		experience = xp;
		rebuild();
		return xp;
	}

	function rebuild() {
		group.clear();
		group.add(0, 0, background);
		if (experience > 0) {
			var h = 40 * hxd.Math.imin(experience, 100) / 100;
			group.addTransform(4, background.height - 4 - h, 4.0, h, 0.0, xpBar);
		}
		group.add(background.width / 2 - portrait.width / 2, background.height / 2 - portrait.height / 2, portrait);
	}
}

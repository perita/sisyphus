class Dead extends h2d.Object {
	public function new(font:h2d.Font, ?parent:h2d.Object) {
		super(parent);
		setPosition(10, 10);

		var backgroundScroll = hxd.Res.tiny_dungeon_interface.get("panel_scroll");
		var background = new h2d.ScaleGrid(backgroundScroll, 16, 16, 16, 16, this);
		background.width = Game.WorldWidth * Game.TileWidth - 20;
		background.height = Game.WorldHeight * Game.TileHeight - 48 - 20;

		var skullTile = hxd.Res.tiny_dungeon_items.get("skull");
		var skull = new h2d.Bitmap(skullTile, background);
		skull.scale(2);
		skull.setPosition(background.width / 2 - skullTile.width, 63);

		var fontColor = 0xff333333;

		var title = new h2d.Text(font, background);
		title.text = "You have been\nslain!";
		title.textColor = fontColor;
		title.textAlign = Center;
		title.maxWidth = background.width;
		title.setPosition(0, 100);

		var prompt = new h2d.Text(font, background);
		prompt.text = "Tap to begin anew";
		prompt.textColor = fontColor;
		prompt.textAlign = Center;
		prompt.maxWidth = background.width;
		prompt.setPosition(0, 140);

		final interactive = new h2d.Interactive(background.width, background.height, this);
		interactive.onPush = (event) -> onClose();

		hxd.Res.human.play();
	}

	public dynamic function onClose() {}
}

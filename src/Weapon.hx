class Weapon extends h2d.Object {
	final background:h2d.Tile;
	final image:h2d.Tile;
	final group:h2d.TileGroup;

	public function new(?parent:h2d.Object) {
		super(parent);
		background = hxd.Res.tiny_dungeon_interface.get("panel_window");
		image = hxd.Res.tiny_dungeon_items.get("weapon_sword");
		group = new h2d.TileGroup(background, this);
		group.add(0, 0, background);
		group.add(background.width / 2 - image.width / 2, background.height / 2 - image.height / 2, image);
	}

	public var height(get, null):Float;
	public var width(get, null):Float;

	function get_height():Float
		return background.height;

	function get_width():Float
		return background.width;
}

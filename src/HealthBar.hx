class HealthBar extends h2d.Object {
	var background:h2d.ScaleGrid;
	var bar:h2d.ScaleGrid;
	var damage:h2d.ScaleGrid;

	public function new(?parent:h2d.Object) {
		super(parent);

		var empty = hxd.Res.tiny_dungeon_interface.get("hp_empty");
		background = new h2d.ScaleGrid(empty, 3, 4, this);
		var full = hxd.Res.tiny_dungeon_interface.get("hp_full");
		bar = new h2d.ScaleGrid(full, 1, 1, this);
		bar.setPosition(2, 4);
		var white = hxd.Res.tiny_dungeon_interface.get("white");
		damage = new h2d.ScaleGrid(full, 1, 1, this);
		damage.blendMode = Add;
		damage.setDefaultColor(0xffffff, 0.5);
		damage.setPosition(2, 4);
	}

	public var height(get, null):Float;
	public var width(get, null):Float;

	public var maxValue(default, set):Float = 100;
	public var value(default, set):Float = 100;
	public var expectedDamage(default, set):Float = 0;
	public var dead(get,null):Bool;

	function get_dead():Bool {
		return hxd.Math.abs(value) < hxd.Math.EPSILON;
	}

	function set_maxValue(v:Float):Float {
		maxValue = v;
		updateBar();
		return v;
	}

	function set_value(v:Float):Float {
		value = hxd.Math.clamp(v, 0, maxValue);
		updateBar();
		return value;
	}

	function set_expectedDamage(v:Float):Float {
		expectedDamage = v;
		updateBar();
		return v;
	}

	function updateBar() {
		bar.visible = value > 0;
		final w = (background.width - 4) * value / maxValue;
		damage.visible = expectedDamage > 0;
		damage.width = hxd.Math.clamp((background.width - 4) * expectedDamage / maxValue, 0, w);
		bar.width = w - damage.width;
		damage.setPosition(bar.x + bar.width, bar.y);
	}

	inline function get_height()
		return background.height;

	inline function get_width()
		return background.width;
}

enum EventKind {
	NextLevel;
	ExploreRoom;
	OpenDoors;
	TakeTheStairs;
	PickupGold(amount:Int);
	Fight(party:Array<Party.Member>);
}

class Event {
	public final kind:EventKind;
	public var level:Level;
	public var room:Level.Room;

	public function new(kind) {
		this.kind = kind;
	}
}

class Dungeon {
	public final rand:hxd.Rand;
	public var currentFloor:Int = 0;

	var level:Level;
	final listeners:Array<Event->Void> = new Array();

	public function new() {
		final seed = Std.random(0x7FFFFFFF);
		trace(seed);
		rand = new hxd.Rand(seed);
	}

	public function addEventListener(l:Event->Void) {
		listeners.push(l);
	}

	public function init() {
		nextLevel();
	}

	public function pushEvent(room:Level.Room, e:Event) {
		e.room = room;
		e.level = level;
		for (listener in listeners) {
			listener(e);
		}
	}

	public function descend(room:Level.Room) {
		final e = new Event(TakeTheStairs);
		pushEvent(room, e);
	}

	public function nextLevel() {
		currentFloor++;
		level = DungeonGenerator.generateLevel(this, 50, 50, currentFloor);
		level.visitRoom(level.startingRoom);
		final e = new Event(NextLevel);
		pushEvent(null, e);
	}

	public function reset() {
		currentFloor = 0;
		init();
	}
}

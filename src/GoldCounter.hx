import h2d.Bitmap;

class GoldCounter extends h2d.Object {
	final text:h2d.Text;

	public function new(font:h2d.Font, ?parent:h2d.Object) {
		super(parent);

		final coins = hxd.Res.tiny_dungeon_items.get("coins_gold");
		final bitmap = new Bitmap(coins, this);
		bitmap.setPosition(-coins.width, 0);

		text = new h2d.Text(font, this);
		text.text = "0";
		text.textAlign = Right;
		text.maxWidth = 200;
		text.setPosition(-coins.width - text.maxWidth, coins.height / 2 - 3);
	}

	public var amount(default, set):Int = 0;

	function set_amount(amount:Int):Int {
		this.amount = amount;
		text.text = Std.string(amount);
		return amount;
	}
}

using DungeonGenerator.RandExtender;
using Party.Enemy;

enum Tile {
	Empty;
	Floor;
	Wall;
	ClosedDoor(room:Room);
	OpenDoor(room:Room);
	StairDownClosed;
	StairDownOpen;
	ChestClosed(amount:Int);
	ChestFull(amount:Int);
	ChestEmpty;
	Enemy(enemy:Enemy, party:Array<Party.Member>);
	Remains();
}

class Room {
	public final x:Int;
	public final y:Int;
	public final width:Int;
	public final height:Int;
	public var seen:Bool = false;
	public var explored:Bool = false;
	public var interactive:h2d.Interactive;

	final level:Level;

	public function new(level:Level, x:Int, y:Int, width:Int, height:Int) {
		this.level = level;
		this.x = x - Std.int(width / 2);
		this.y = y - Std.int(height / 2);
		this.width = width;
		this.height = height;
	}

	public var content(default, set):Tile = Floor;

	function set_content(t:Tile):Tile {
		content = t;
		level.cellAt(centerY, centerX).tile = t;
		return t;
	}

	public var centerX(get, null):Int;
	public var centerY(get, null):Int;

	function get_centerX():Int {
		return x + Std.int(width / 2);
	}

	function get_centerY():Int {
		return y + Std.int(height / 2);
	}

	public var done(get, null):Bool;

	function get_done():Bool {
		return explored && switch (content) {
			case StairDownClosed, StairDownOpen, ChestClosed(_), ChestFull(_), Enemy(_): false;
			case Floor, Empty, Wall, ClosedDoor(_), OpenDoor(_), ChestEmpty, Remains: true;
		};
	}

	public function explore() {
		explored = true;
		switch (content) {
			case StairDownClosed:
				content = StairDownOpen;
				level.openDoors(this);
			case StairDownOpen:
				level.takeTheStairs(this);
			case ChestClosed(amount):
				hxd.Res.chest_open.play();
				content = ChestFull(amount);
				level.openDoors(this);
			case ChestFull(amount):
				hxd.Res.gold.play();
				content = ChestEmpty;
				level.pickupGold(this, amount);
			case ChestEmpty | Remains: // Nothing to do
			case Floor:
				level.openDoors(this);
			case Enemy(_, party):
				level.fight(this, party);
			case Empty, Wall, ClosedDoor(_), OpenDoor(_):
				trace('Invalid floor content: ${content}');
		}
	}

	public function killEnemies() {
		content = Remains;
		level.openDoors(this);
	}

	public var hasTreasure(get, null):Bool;
	public var treasureFound(get, null):Bool;
	public var hasEnemies(get, null):Bool;
	public var hasDefeatedEnemies(get, null):Bool;

	function get_hasTreasure()
		return switch (content) {
			case ChestClosed(_), ChestFull(_), ChestEmpty: true;
			default: false;
		}

	function get_treasureFound()
		return content == ChestEmpty;

	function get_hasEnemies()
		return content.match(Enemy(_)) || content.match(Remains);

	function get_hasDefeatedEnemies()
		return content.match(Remains);
}

class Cell {
	public var tile:Tile;
	public var seen:Bool;
	public final variation:Int;

	public function new(variation:Int) {
		this.tile = Empty;
		this.variation = variation;
		seen = false;
	}
}

class Level {
	final dungeon:Dungeon;

	public final width:Int;
	public final height:Int;
	public final rooms:Array<Room>;
	public final cells:Array<Cell>;

	public function new(dungeon:Dungeon, width:Int, height:Int) {
		this.dungeon = dungeon;
		this.width = width;
		this.height = height;
		rooms = new Array<Room>();
		final rand = dungeon.rand;
		cells = [
			for (y in 0...height)
				for (x in 0...width)
					new Cell(rand.randBoolean(0.85) ? 0 : rand.range(1, 5))
		];
	}

	public var startingRoom(get, null):Room;

	private function get_startingRoom() {
		return rooms[0];
	}

	public function addRoom(room:Room) {
		rooms.push(room);
		for (y in 0...room.height) {
			for (x in 0...room.width) {
				cellAt(room.y + y, room.x + x).tile = Floor;
			}
		}
	}

	public inline function cellAt(y:Int, x:Int):Cell {
		return cells[y * width + x];
	}

	public inline function isWall(y:Int, x:Int):Bool {
		return y < height && cellAt(y, x).tile == Wall;
	}

	public inline function isFloor(y:Int, x:Int):Bool {
		return y < height && cellAt(y, x).tile == Floor;
	}

	public function finish() {
		addWalls();
		for (room in rooms) {
			addDoors(room);
		}
		addStairDown(rooms[rooms.length - 1]);
	}

	private function addWalls() {
		for (y in 0...height) {
			for (x in 0...width) {
				if (isFloor(y, x)) {
					maybeAddWall(y - 1, x - 1);
					maybeAddWall(y - 1, x + 1);
					maybeAddWall(y + 1, x - 1);
					maybeAddWall(y + 1, x + 1);
				}
			}
		}
	}

	private inline function maybeAddWall(y:Int, x:Int) {
		final cell = cellAt(y, x);
		if (cell.tile == Empty) {
			cell.tile = Wall;
		}
	}

	private function addDoors(room:Room) {
		for (y in 0...room.height) {
			maybeAddDoor(cellAt(room.y + y, room.x - 1), room);
			maybeAddDoor(cellAt(room.y + y, room.x + room.width), room);
		}
		for (x in 0...room.width) {
			maybeAddDoor(cellAt(room.y - 1, room.x + x), room);
			maybeAddDoor(cellAt(room.y + room.height, room.x + x), room);
		}
	}

	private inline function maybeAddDoor(cell:Cell, room:Room) {
		if (cell.tile == Floor) {
			cell.tile = ClosedDoor(room);
		}
	}

	private function addStairDown(room:Room) {
		room.content = StairDownClosed;
	}

	public function visitRoom(room:Room) {
		for (y in -1...room.height + 1) {
			for (x in -1...room.width + 1) {
				cellAt(room.y + y, room.x + x).seen = true;
			}
		}
		room.seen = true;
		final event = new Dungeon.Event(Dungeon.EventKind.ExploreRoom);
		dungeon.pushEvent(room, event);
	}

	public function fight(room:Room, party:Array<Party.Member>) {
		final event = new Dungeon.Event(Dungeon.EventKind.Fight(party));
		dungeon.pushEvent(room, event);
	}

	public function openDoors(room:Room) {
		hxd.Res.door.play();
		for (y in 0...room.height) {
			maybeOpenDoor(room.y + y, room.x - 1);
			maybeOpenDoor(room.y + y, room.x + room.width);
		}
		for (x in 0...room.width) {
			maybeOpenDoor(room.y - 1, room.x + x);
			maybeOpenDoor(room.y + room.height, room.x + x);
		}
		final event = new Dungeon.Event(Dungeon.EventKind.OpenDoors);
		dungeon.pushEvent(room, event);
	}

	public function takeTheStairs(room:Room) {
		dungeon.descend(room);
	}

	public function maybeOpenDoor(y:Int, x:Int) {
		final cell = cellAt(y, x);
		switch (cell.tile) {
			case ClosedDoor(room):
				cell.tile = OpenDoor(room);
				cell.seen = true;
				visitCell(y + 1, x);
				visitCell(y - 1, x);
				visitCell(y, x + 1);
				visitCell(y, x - 1);
			default:
		}
	}

	function visitCell(y:Int, x:Int) {
		if (y < 0 || y >= height || x < 0 || x >= width)
			return;
		final cell = cellAt(y, x);
		final end = cell.seen;
		cell.seen = true;
		switch (cell.tile) {
			case Empty, Wall:
			case ClosedDoor(room):
				cell.tile = OpenDoor(room);
				if (!room.seen) {
					visitRoom(room);
				}
			case Floor, StairDownClosed, StairDownOpen, OpenDoor(_), ChestFull(_), ChestClosed(_), ChestEmpty, Enemy(_), Remains:
				if (end)
					return;
				for (i in -1...2) {
					for (j in -1...2) {
						visitCell(y + i, x + j);
					}
				}
		}
	}

	public function pickupGold(room:Room, amount:Int) {
		final event = new Dungeon.Event(PickupGold(amount));
		dungeon.pushEvent(room, event);
	}

	public function countTotalRooms()
		return rooms.length;

	public function countExploredRooms()
		return rooms.filter(r -> return r.explored).length;

	public function countTotalTreasures()
		return rooms.filter(r -> r.hasTreasure).length;

	public function countFoundTreasures()
		return rooms.filter(r -> r.treasureFound).length;

	public function countEnemiesDefeated()
		return rooms.filter(r -> r.hasDefeatedEnemies).length;

	public function countTotalEnemies()
		return rooms.filter(r -> r.hasEnemies).length;
}

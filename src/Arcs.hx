import h2d.TileGroup;

class Arcs extends h2d.Object {
	final bg:h2d.Tile;
	final fg:h2d.Tile;
	final group:h2d.TileGroup;

	public function new(?parent:h2d.Object) {
		super(parent);

		bg = hxd.Res.pixel_dungeon_arcs1.toTile();
		fg = hxd.Res.pixel_dungeon_arcs2.toTile();
		group = new h2d.TileGroup(bg, this);
	}

	public function setSize(width:Float, height:Float) {
		final alpha = 0.20;
		group.clear();
		for (pos in new TileIterator(width, height, bg)) {
			group.addAlpha(pos.x, pos.y, alpha, bg);
		}
		for (pos in new TileIterator(width, height, fg)) {
			group.addAlpha(pos.x, pos.y, alpha, fg);
		}
	}
}

private class TileIterator {
	final width:Float;
	final height:Float;
	final stepX:Float;
	final stepY:Float;
	var x:Float;
	var y:Float;

	public function new(width:Float, height:Float, tile:h2d.Tile) {
		this.width = width;
		this.height = height;
		this.stepX = tile.width;
		this.stepY = tile.height;
		x = 0.0;
		y = 0.0;
	}

	public inline function hasNext() {
		return y < height;
	}

	public inline function next() {
		final pos = {x: x, y: y};
		x += stepX;
		if (x >= width) {
			x = 0.0;
			y += stepY;
		}
		return pos;
	}
}

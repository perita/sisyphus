using Level.Tile;

class Game extends hxd.App {
	public static inline final WorldWidth = 10;
	public static inline final WorldHeight = 18;
	public static inline final TileWidth = 16;
	public static inline final TileHeight = 16;
	public static inline final AnimSpeed = 2;

	var levelGroup:h2d.TileGroup;
	var dungeon:Dungeon;
	var animations:Array<h2d.Anim> = new Array();
	var floorText:h2d.Text;
	var font:h2d.Font;
	var canDrag:Bool = false;
	var tiles:hxd.res.Atlas;
	var monsters:hxd.res.Atlas;
	var gold:GoldCounter;
	var sisyphus:Sisyphus;

	override function init() {
		super.init();

		tiles = hxd.Res.tiny_dungeon_world;
		monsters = hxd.Res.tiny_dungeon_monsters;

		dungeon = new Dungeon();
		dungeon.addEventListener(onDungeonEvent);

		s2d.scaleMode = LetterBox(WorldWidth * TileWidth, WorldHeight * TileHeight);

		final arcs = new Arcs(s2d);
		var resizeArcs = () -> {
			final w = hxd.Window.getInstance();
			arcs.setSize(w.width, w.height);
			arcs.setPosition(s2d.width / 2 - w.width / 2, s2d.height / 2 - w.height / 2);
		};
		hxd.Window.getInstance().addResizeEvent(resizeArcs);
		resizeArcs();

		levelGroup = new h2d.TileGroup(null, s2d);
		levelGroup.scale(0.5);

		sisyphus = new Sisyphus(dungeon.rand);
		s2d.addChild(sisyphus.portrait);
		sisyphus.portrait.setPosition(0, s2d.height - sisyphus.portrait.height);

		final weapon = new Weapon(s2d);
		weapon.setPosition(s2d.width - weapon.width, s2d.height - weapon.height);

		s2d.addChild(sisyphus.health);
		sisyphus.health.setPosition(s2d.width / 2 - sisyphus.health.width / 2, s2d.height - sisyphus.health.height - 5);

		font = hxd.Res.tiny_dungeon_font.toFont();

		gold = new GoldCounter(font, s2d);
		gold.setPosition(s2d.width, 5);

		floorText = new h2d.Text(font, s2d);
		floorText.textAlign = Center;
		floorText.maxWidth = s2d.width + 7;
		floorText.setPosition(0, s2d.height - 35);

		s2d.addEventListener(this.onEvent);
		dungeon.init();
	}

	private function setupRoom(room:Level.Room) {
		room.interactive = new h2d.Interactive((room.width + 2) * TileWidth, (room.height + 2) * TileHeight, levelGroup);
		room.interactive.setPosition((room.x - 1) * TileWidth, (room.y - 1) * TileHeight);
		room.interactive.onClick = e -> {
			room.explore();
		};
	}

	private function onEvent(event:hxd.Event) {
		switch (event.kind) {
			case EPush:
				if (!canDrag)
					return;
				var prevX = event.relX;
				var prevY = event.relY;
				s2d.startCapture(event -> {
					if (event.kind != EMove)
						return;
					event.propagate = false;
					var dx = (event.relX - prevX) / s2d.viewportScaleX / levelGroup.scaleX;
					var dy = (event.relY - prevY) / s2d.viewportScaleY / levelGroup.scaleY;
					levelGroup.setPosition(levelGroup.x + dx, levelGroup.y + dy);
					prevX = event.relX;
					prevY = event.relY;
				});
			case ERelease, EReleaseOutside, EOut, EFocusLost:
				s2d.stopCapture();
			case ECheck:
			case EMove:
			case EFocus:
			case EKeyDown:
			case EKeyUp:
			case EOver:
			case ETextInput:
			case EWheel:
		}
	}

	function onDungeonEvent(event:Dungeon.Event) {
		switch (event.kind) {
			case NextLevel:
				canDrag = true;
				centerOnRoom(event.level.startingRoom);
				floorText.visible = true;
				floorText.text = '${dungeon.currentFloor}F';
				hxd.Res.explore.play(true);
			case ExploreRoom:
				rebuildLevel(event.level);
				setupRoom(event.room);
			case OpenDoors:
				rebuildLevel(event.level);
			case TakeTheStairs:
				canDrag = false;
				final level = event.level;
				for (room in level.rooms) {
					removeInteractive(room);
				}
				floorText.visible = false;
				hxd.Res.explore.stop();
				hxd.Res.win.play();
				final summary = new Summary(level.countExploredRooms(), level.countTotalRooms(), level.countFoundTreasures(), level.countTotalTreasures(),
					level.countEnemiesDefeated(), level.countTotalEnemies(), font, s2d);
				summary.onClose = () -> {
					summary.remove();
					hxd.Res.win.stop();
					dungeon.nextLevel();
				}
			case PickupGold(amount):
				gold.amount += amount;
				rebuildLevel(event.level);
			case Fight(party):
				canDrag = false;
				final level = event.level;
				for (room in level.rooms) {
					disableInteractive(room);
				}
				hxd.Res.explore.stop();
				hxd.Res.battle.play(true);
				final fight = new Fight(sisyphus, party, font, dungeon.rand, s2d);
				fight.onClose = () -> {
					hxd.Res.battle.stop();
					fight.remove();
					if (sisyphus.dead) {
						hxd.Res.lose.play();
						final dead = new Dead(font, s2d);
						dead.onClose = () -> {
							hxd.Res.lose.stop();
							gold.amount = 0;
							dead.remove();
							dungeon.reset();
							sisyphus.reset();
						}
					} else {
						hxd.Res.explore.play(true);
						for (room in level.rooms) {
							enableInteractive(room);
						}
						canDrag = true;
						event.room.killEnemies();
					}
				};
		}
		final room = event.room;
		if (room != null && room.done) {
			removeInteractive(event.room);
		}
	}

	function centerOnRoom(room:Level.Room) {
		levelGroup.setPosition(s2d.width / 2 - room.centerX * TileWidth / 2, s2d.height / 2 - room.centerY * TileWidth / 2);
	}

	function removeInteractive(room:Level.Room) {
		if (room.interactive != null) {
			room.interactive.cursor = Default;
			room.interactive.remove();
			room.interactive = null;
		}
	}

	function disableInteractive(room:Level.Room) {
		if (room.interactive != null) {
			room.interactive.visible = false;
		}
	}

	function enableInteractive(room:Level.Room) {
		if (room.interactive != null) {
			room.interactive.visible = true;
		}
	}

	function rebuildLevel(level:Level) {
		while (animations.length > 0) {
			animations.pop().remove();
		}
		final monster = [
			Party.Enemy.Rat => monsters.getAnim("rat_d"),
			Party.Enemy.Bat => monsters.getAnim("bat_d"),
			Party.Enemy.Beetle => monsters.getAnim("beetle_d"),
			Party.Enemy.Jelly => monsters.getAnim("jelly_d"),
			Party.Enemy.Slime => monsters.getAnim("slime_d"),
			Party.Enemy.Snake => monsters.getAnim("snake_d"),
			Party.Enemy.Spider => monsters.getAnim("spider_d"),
		];

		var torch = tiles.getAnim("torch");
		var torchLight = tiles.getAnim("torch_light");
		var wallH = [
			tiles.get("wall_stone_h_a"),
			tiles.get("wall_stone_h_b"),
			tiles.get("wall_stone_h_c"),
			tiles.get("wall_stone_h_d"),
			tiles.get("wall_stone_h_crack"),
			tiles.get("wall_stone_h_b"),

		];
		var wallV = [
			tiles.get("wall_stone_v_a"),
			tiles.get("wall_stone_v_b"),
			tiles.get("wall_stone_v_c"),
			tiles.get("wall_stone_v_d"),
			tiles.get("wall_stone_v_crack"),
			tiles.get("wall_stone_v_b"),
		];
		var closedDoorH = tiles.get("door_wood_h_closed");
		var openDoorH = tiles.get("door_wood_h_open");
		var closedDoorV = [tiles.get("door_wood_v_closed1"), tiles.get("door_wood_v_closed2"),];
		var openDoorV = [tiles.get("door_wood_v_open2"), tiles.get("door_wood_v_open1"),];
		var floor = [
			tiles.get("floor_cobble_a"),
			tiles.get("floor_cobble_b"),
			tiles.get("floor_cobble_c"),
			tiles.get("floor_cobble_d"),
			tiles.get("floor_cobble_e"),
			tiles.get("floor_cobble_f"),
		];
		var remains = [
			tiles.get("blood_a"),
			tiles.get("blood_b"),
			tiles.get("blood_bone"),
			tiles.get("bones_a"),
			tiles.get("bones_b"),
			tiles.get("bones_c"),
		];
		var shadow = tiles.get("shadow");
		var stairDown = tiles.get("wall_stone_stair_down");
		var trapdoorClosed = tiles.get("trapdoor_closed");
		var trapdoorOpen = [tiles.get("trapdoor_open1"), tiles.get("trapdoor_open2"),];
		var chestClosed = tiles.get("chest");
		var chestEmpty = tiles.get("chest_empty");
		var chestFull = tiles.get("chest_full");

		levelGroup.clear();
		for (y in 0...level.height) {
			for (x in 0...level.width) {
				var cell = level.cellAt(y, x);
				if (!cell.seen) {
					continue;
				}
				var tile:h2d.Tile;
				switch (cell.tile) {
					case Wall:
						tile = level.isWall(y + 1, x) ? wallV[cell.variation] : wallH[cell.variation];
					case Floor, Remains, ClosedDoor(_), OpenDoor(_), ChestClosed(_), ChestFull(_), ChestEmpty, Enemy(_):
						tile = floor[cell.variation];
					case StairDownOpen:
						tile = stairDown;
					case StairDownClosed:
						tile = trapdoorClosed;
					case Empty:
						continue;
				}
				final pixelX = x * TileWidth;
				final pixelY = y * TileHeight;
				levelGroup.add(pixelX, pixelY, tile);
				switch (cell.tile) {
					case ClosedDoor(_):
						if (level.isFloor(y + 1, x)) {
							levelGroup.add(pixelX, pixelY, closedDoorH);
						} else {
							levelGroup.add(pixelX, pixelY - TileHeight, closedDoorV[1]);
							levelGroup.add(pixelX, pixelY, closedDoorV[0]);
						}
					case Floor:
						if (level.isWall(y - 1, x)) {
							levelGroup.add(pixelX, pixelY, shadow);
							if (cell.variation == 5) {
								addAnimation(pixelX, pixelY - TileHeight, torch, AnimSpeed * 2);
								addAnimation(pixelX, pixelY, torchLight, AnimSpeed * 2);
							}
						}
					case Remains:
						levelGroup.add(pixelX, pixelY, remains[cell.variation]);
					case OpenDoor(room):
						if (level.isWall(y - 1, x)) {
							levelGroup.add(pixelX, pixelY, shadow);
						}
						if (level.isFloor(y + 1, x)) {
							levelGroup.add(pixelX, pixelY, openDoorH);
						} else {
							final transform = room.x > x ? {scale: -1, offset: TileWidth} : {scale: 1, offset: 0};
							levelGroup.addTransform(pixelX + transform.offset, pixelY - TileHeight, transform.scale, 1, 0, openDoorV[1]);
							levelGroup.addTransform(pixelX + transform.offset, pixelY, transform.scale, 1, 0, openDoorV[0]);
						}
					case StairDownOpen:
						levelGroup.add(pixelX, pixelY - TileWidth, trapdoorOpen[1]);
						levelGroup.add(pixelX, pixelY, trapdoorOpen[0]);
					case ChestClosed(_):
						levelGroup.add(pixelX, pixelY, chestClosed);
					case ChestFull(_):
						levelGroup.add(pixelX, pixelY, chestFull);
					case ChestEmpty:
						levelGroup.add(pixelX, pixelY, chestEmpty);
					case Enemy(enemy, _):
						addAnimation(pixelX, pixelY, monster[enemy]);
					case Wall, StairDownClosed, Empty:
				}
			}
		}
	}

	function addAnimation(x:Float, y:Float, frames:Array<h2d.Tile>, ?speed = AnimSpeed) {
		final anim = new h2d.Anim(frames, speed, levelGroup);
		anim.setPosition(x, y);
		animations.push(anim);
	}

	static function main() {
		#if js
		hxd.Res.initEmbed();
		#else
		hxd.Res.initLocal();
		#end
		new Game();
	}
}
